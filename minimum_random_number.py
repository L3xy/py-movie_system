# Generating 10 random number between 1 & 20

import random

min = 20

for num in range(10):   # run 10 times
    rand_num = random.randint(0, 20)
    if rand_num <= min:
        min = rand_num
    print(rand_num)
print('The minimum number generated ids {}'.format(min))